USE Master

CREATE DATABASE Хранилище
GO

USE Контейнер
GO

CREATE TABLE Склад(
	Номер int PRIMARY KEY IDENTITY(1, 1),
	[Адрес склада] nvarchar(50) NOT NULL,
	Телефон char(11) NULL,
	[Фамилия руководителя] nvarchar(50) NULL,
	UNIQUE([Адрес склада], Телефон, [Фамилия руководителя])
)
GO

CREATE TABLE Товар(
	Код int PRIMARY KEY IDENTITY(1, 1),
	Название nvarchar(50),
	[Группа товаров] nvarchar(50) DEFAULT 'Разное',
	[Фирма-производитель] nvarchar(50) NULL,
	UNIQUE (Название, [Группа товаров], [Фирма-производитель])
)
GO

CREATE TABLE НаличиеТоваров(
	[Номер склада] int,
	[Код товара] int,
	Количество int CHECK (Количество >= 0),
	PRIMARY KEY ([Номер склада], [Код товара]),
	FOREIGN KEY ([Номер склада]) REFERENCES Склад(Номер),
	FOREIGN KEY ([Код товара]) REFERENCES Товар(Код)
)
GO

